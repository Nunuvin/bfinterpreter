#avaliable under GPL v3 or later
'''
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
import sys

def lexer(code):
    '''
        converts code into tokens.
    '''
    code=code.replace(' ','')
    tokens=[]
    for token in code:
        if token=='>' : tokens.append('>')
        elif token=='<' : tokens.append('<')
        elif token=='+' : tokens.append('+')
        elif token=='-' : tokens.append('-')
        elif token=='.' : tokens.append('.')
        elif token==',' : tokens.append(',')
        elif token=='[' : tokens.append('[')
        elif token==']' : tokens.append(']')
    return tokens

def parser(memory,tokens):
    #mods memory based on tokens provided
    def interpret(memory,index,tok):
        #acts on specific token provided by changing memory at index  or index
        if tok=='>' : index+=1 
        elif tok=='<' : index-=1
        elif tok=='+' : memory[index]+=1
        elif tok=='-' : memory[index]-=1
        elif tok==',' : memory[index]=int(input())
        elif tok=='.' : print(chr(memory[index]))
        return memory,index 
    ind=0
    loopStarted=0
    startloop=[]
    skip=0
    index=0
    while ind<len(tokens):
        if tokens[ind] =='[' and memory[index]!=0:
            startloop.append(ind)
            loopStarted+=1
        elif tokens[ind] =='[' and memory[index]==0:
            loopStarted+=1
            skip=loopStarted
        elif tokens[ind]==']' and memory[index]!=0:
            ind=startloop[-1]
        elif tokens[ind]==']' and memory[index]<=0:
            del startloop[-1]
            loopStarted-=1
            if skip!=0:
                skip-=1
        elif skip==0: 
            memory,index=interpret(memory,index,tokens[ind])
        ind+=1

        


def main():
    if len(sys.argv)==3:
        #1-program itself, mem limit, bf source file
        mem=[0 for i in range(int(sys.argv[1]))]
        with open(sys.argv[2],'r') as fileIn:
            source=fileIn.read()
        tokenStream=lexer(source)
        parser(mem,tokenStream)
    else:
        print('BS! bf.py MemoryLimit bfFile ')

main()